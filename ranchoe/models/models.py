# -*- coding: utf-8 -*-

from odoo import models, fields, api

## MASTER MODEL
class Role (models.Model):
	_name = 'ranchoe.role'
	name = fields.Char(required=True)
	description = fields.Text()

class FeedType(models.Model):
	_name = 'ranchoe.feed_types'

	name = fields.Char(required=True)
	description = fields.Text()

class UnitOfMeasure (models.Model):
	_name = 'ranchoe.unit_of_measure'
	name = fields.Char(required=True)	
	short_name = fields.Char()

class ProductType (models.Model):
	_name = 'ranchoe.product_type'

	name = fields.Char(required=True)
	other_name = fields.Char()
	description = fields.Text()

class Species(models.Model):
	_name = 'ranchoe.species'

	name = fields.Char(required=True)
	latin_name = fields.Char()
	description = fields.Text()

## RANCH MODEL
class Officer (models.Model):
	_name = 'ranchoe.officer'

	code = fields.Char(required=True)
	name = fields.Char()
	address = fields.Char()
	phone = fields.Char()
	email = fields.Char()
	about = fields.Text()
	role_id = fields.Many2one('ranchoe.role', ondelete='cascade', string="Set Role", required=True)

class Cage (models.Model):
	_name = 'ranchoe.cage'
	cage_type_list = (
			('individual', 'Individual'),
			('comunal', 'Comunal'),
			('mix', 'Mix'),
		)

	code = fields.Char(required=True)
	name = fields.Char()
	capacity = fields.Integer()
	height = fields.Float()
	width = fields.Float()
	length = fields.Float()
	types = fields.Selection(selection=cage_type_list)
	description = fields.Text()
	
class Feed (models.Model):
	_name = 'ranchoe.feed'

	code = fields.Char(required=True)
	name = fields.Char()
	description = fields.Text()
	feed_type_id = fields.Many2one('ranchoe.feed_types', ondelete='cascade', string="Feed Type", required=True)

class Animal (models.Model):
	_name = 'ranchoe.animal'

	code = fields.Char(required=True)
	name = fields.Char()
	description = fields.Text()
	species_id = fields.Many2one('ranchoe.species', ondelete='cascade', string="Set Species", required=True)
	cage_id = fields.Many2one('ranchoe.cage', ondelete='cascade', string="Set Cage", required=True)

##RANCH ACTIVITY MODULE
class FeedingLog (models.Model):
	_name = 'ranchoe.feeding_log'

	code = fields.Char(required=True)
	note = fields.Text()
	feeding_time = fields.Datetime()
	amount = fields.Integer()
	feed_id = fields.Many2one('ranchoe.feed', ondelete='cascade', string="Feed", required=True)
	uom_id = fields.Many2one('ranchoe.unit_of_measure', ondelete='cascade', string="Unit of Measure", required=True)
	animal_id = fields.Many2one('ranchoe.animal', ondelete='cascade', string="Animal", required=True)
	officer_id = fields.Many2many('ranchoe.officer', string="Officer")

class WeightingLog (models.Model):
	_name = 'ranchoe.weighting_log'

	code = fields.Char(required=True)
	note = fields.Text()
	weighting_time = fields.Date()
	weight_initial = fields.Float()
	weight_final = fields.Float()
	weight_difference = fields.Float(compute="_weight_diff", store=True)
	uom_id = fields.Many2one('ranchoe.unit_of_measure', ondelete='cascade', string="Unit of Measure", required=True)
	animal_id = fields.Many2one('ranchoe.animal', ondelete='cascade', string="Animal", required=True)
	officer_id = fields.Many2many('ranchoe.officer', string="Officer")

	@api.depends('weight_final')
	def _value_pc(self):
	    self.weight_difference =  float(self.weight_final) - float(self.weight_initial)

class ProductLog (models.Model):
	_name = 'ranchoe.product_log'

	code = fields.Char(required=True)
	name = fields.Char()
	note = fields.Text()
	production_time = fields.Date()
	amount = fields.Integer()
	product_type_id = fields.Many2one('ranchoe.product_type', ondelete='cascade', string="Product Type", required=True)
	uom_id = fields.Many2one('ranchoe.unit_of_measure', ondelete='cascade', string="Unit of Measure", required=True)
	animal_id = fields.Many2one('ranchoe.animal', ondelete='cascade', string="Animal", required=True)
	officer_id = fields.Many2many('ranchoe.officer', string="Officer")

## Domba Tanduk Model