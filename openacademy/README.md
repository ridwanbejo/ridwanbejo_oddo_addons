#How to Develop Odoo Module

- Download Odoo dengan mengklon dari Github: git clone <odoo_repository> --depth=1
- Buat virtualenv untuk odoo kemudian pasang beberapa modul python seperti lxml, reportlab, ldap dan lainnya
- Install requirements odoo dari file requirements.txt yang ada di dalam repo odoo: pip install -r requirements.txt
- Buat role baru pada PostgreSQL: createuser -s nama_pengguna
- Masuk ke dalam PostgreSQL: su nama_pengguna && psql
- Beri password untuk role tersebut: ALTER USER username WITH PASSWORD 'password';
- Jalankan odoo-bin dan ikuti instalasi
