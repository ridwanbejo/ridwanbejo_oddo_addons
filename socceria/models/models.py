# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Player(models.Model):
	_name = 'socceria.player'

	name = fields.Char(required=True)
	dob = fields.Date()
	nationality = fields.Char()
	height = fields.Float()
	weight = fields.Float()
	about = fields.Text()
	team_squad_ids = fields.One2many('socceria.team_squad', 'player_id', string='Team Squads')

class Coach(models.Model):
	_name = 'socceria.coach'

	name = fields.Char(required=True)
	dob = fields.Date()
	nationality = fields.Char()
	license = fields.Char()
	about = fields.Text()
	team_ids = fields.One2many('socceria.team', 'coach_id', string='Teams')

class Team(models.Model):
	_name = 'socceria.team'

	name = fields.Char(required=True)
	established = fields.Date()
	about = fields.Text()
	coach_id = fields.Many2one('socceria.coach', ondelete='cascade', string="Coach", required=True)
	team_squad_ids = fields.One2many('socceria.team_squad', 'team_id', string='Team Squad')
	sessionplan_ids = fields.One2many('socceria.session_plan', 'team_id', string='Session Plan')

class TeamSquad(models.Model):
	_name = 'socceria.team_squad'

	position = (
	    	('GK', 'Goal Keeper'),
	    	('SW', 'Sweeper'),
	    	('CD', 'Central Defender'),
	    	('FB', 'Full backs'),
	    	('WB', 'Wing Backs'),
	    	('DM', 'Defensive Midfielder'),
	    	('AM', 'Attacking Midfielder'),
	    	('SM', 'Side Midfielder / Winger'),
	    	('WF', 'Wing Forward'),
	    	('SS', 'Support Striker'),
	    	('CF', 'Center Forward'),
	    )

	squad_no = fields.Char(required=True)
	position = fields.Selection(selection=position)
	team_id = fields.Many2one('socceria.team', ondelete='cascade', string="Team", required=True)
	player_id = fields.Many2one('socceria.player', ondelete='cascade', string="Player", required=True)


class TrainingVenue(models.Model):
	_name = 'socceria.training_venue'

	name = fields.Char(required=True)
	address = fields.Char()
	about = fields.Text()

class SessionPlan(models.Model):
	_name = 'socceria.session_plan'

	name = fields.Char(required=True)
	emphasis = fields.Char()
	note = fields.Text()
	start_time = fields.Datetime()
	end_time = fields.Datetime()
	team_id = fields.Many2one('socceria.team', ondelete='cascade', string="Team", required=True)
	session_plan_detail_ids = fields.One2many('socceria.session_plan_detail', 'sessionplan_id', string='Session Plan')
	session_plan_agenda_ids = fields.One2many('socceria.session_plan_agenda', 'sessionplan_id', string='Session Agenda')
		
class SessionPlanDetail(models.Model):
	_name = 'socceria.session_plan_detail'

	exercise = fields.Char(required=True)
	keypoint = fields.Text()
	objective = fields.Text()
	sessionplan_id = fields.Many2one('socceria.session_plan', ondelete='cascade', string="Session Plan", required=True)
	
class SessionPlanAgenda(models.Model):
	_name = 'socceria.session_plan_agenda'

	drill = fields.Char(required=True)
	start_time = fields.Datetime()
	end_time = fields.Datetime()
	comment = fields.Text()
	sessionplan_id = fields.Many2one('socceria.session_plan', ondelete='cascade', string="Session Plan", required=True)
	

