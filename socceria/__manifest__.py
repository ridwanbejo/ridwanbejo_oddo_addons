# -*- coding: utf-8 -*-
{
    'name': "Socceria",

    'summary': """
        Session plan apps for soccer academy
    """,

    'author': "POSS UPI",
    'website': "http://poss.cs.upi.edu",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/master.xml',
        'views/team.xml',
        'views/session_plan.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}