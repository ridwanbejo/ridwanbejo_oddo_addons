Koleksi modul yang saya buat untuk Odoo 10:

* Open Academy, modul Odoo yang dibuat berdasarkan tutorial "Building Odoo Module" di dokumentasi resmi Odoo. Sebuah aplikasi yang mencatat perkuliahan dan materi yang disampaikan
* Socceria, modul Odoo yang saya buat untuk mencatat detail latihan sebuah tim sepakbola di dalam klub sepakbola atau sekolah sepak bola
* Ranchoe, modul Odoo yang saya buat untuk mengelola suatu peternakan kelas menengah kebawah

odoo command line snippet:
```
$ odoo --addons odoo/addons,my_odoo_addons --update=socceria
```